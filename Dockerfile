FROM golang:alpine

WORKDIR /app

COPY go.mod go.sum ./
RUN go mod download
COPY . .

RUN go build -o main ./src/
RUN ls -lrt
RUN cat go.sum
EXPOSE 8050
CMD ["./main"]
