package main

import (
	"encoding/xml"
	"github.com/google/uuid"
	"io/ioutil"
	"net/http"
	"sync/atomic"
	"fmt"
)
type SoapRequest struct {
	XMLName xml.Name `xml:"Envelope"`
	Text    string   `xml:",chardata"`
	Soapenv string   `xml:"soapenv,attr"`
	Com     string   `xml:"com,attr"`
	Header  struct {
		Text             string `xml:",chardata"`
		MessageSignature struct {
			Text      string `xml:",chardata"`
			Signature string `xml:"Signature"`
		} `xml:"MessageSignature"`
	} `xml:"Header"`
	Body struct {
		Text                  string `xml:",chardata"`
		GetUserIdFromIdentity struct {
			Text    string `xml:",chardata"`
			Request struct {
				Text         string `xml:",chardata"`
				Username     string `xml:"username"`
				Password     string `xml:"password"`
				Identity     string `xml:"identity"`
				IdentityType string `xml:"identityType"`
				NetworkId    string `xml:"networkId"`
				Bango        string `xml:"bango"`
			} `xml:"request"`
		} `xml:"GetUserIdFromIdentity"`
	} `xml:"Body"`
}

func soap(inMem *atomic.Value) http.HandlerFunc {
	return func(w http.ResponseWriter, req *http.Request) {
		body, _ := ioutil.ReadAll(req.Body)
		var soap SoapRequest
		_ = xml.Unmarshal(body, &soap)

		btId := soap.Body.GetUserIdFromIdentity.Request.Identity
		mapper := inMem.Load().(map[string]string)
		bangoId, ok := mapper[btId]

		if !ok {
			bangoId = uuid.New().String()
			mapper[btId] = bangoId
			inMem.Store(mapper)
		}
		response := fmt.Sprintf(`<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/"> 		   
		<soap:Body xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"> 			  
		<GetUserIdFromIdentityResponse xmlns="com.bango.webservices.directbilling"> 				 
		<GetUserIdFromIdentityResult> 					
		<responseCode>OK</responseCode> 					
		<responseMessage>Invalid username/password</responseMessage> 					
		<userId>%s</userId> 				 
		</GetUserIdFromIdentityResult> 			 
		</GetUserIdFromIdentityResponse> 		  
		</soap:Body> 		
		</soap:Envelope>`, bangoId)
		w.Write([]byte(response))
	}
}

func main() {
	inMem := &atomic.Value{}
	mapper := make(map[string]string)
	mapper["1531132325"] = "1531132325"
	mapper["1570504805"] = "1570504805"
	inMem.Store(mapper)
	http.HandleFunc("/test", soap(inMem))

	http.ListenAndServe(":8050", nil)

}
